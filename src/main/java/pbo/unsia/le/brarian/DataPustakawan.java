/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package pbo.unsia.le.brarian;

/**
 *
 * @author syarifmaulana
 */

import javax.swing.JOptionPane;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.table.DefaultTableModel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class DataPustakawan extends javax.swing.JFrame {

    /**
     * Creates new form DataPustakawan
     */
    
    private Object globalPustakawanId;

    public DataPustakawan() {
        initComponents();
        DBConnection.openConnection();
        LoadPustakawan();
        this.setDefaultCloseOperation(PeminjamanBuku.DISPOSE_ON_CLOSE);
    }
    
    private void setColumnWidth() {
        TablePustakawan.getColumnModel().getColumn(0).setPreferredWidth(30);
        TablePustakawan.getColumnModel().getColumn(1).setPreferredWidth(30);
        TablePustakawan.getColumnModel().getColumn(2).setPreferredWidth(100);
        TablePustakawan.getColumnModel().getColumn(3).setPreferredWidth(100);
        TablePustakawan.getColumnModel().getColumn(4).setPreferredWidth(100);
        TablePustakawan.getColumnModel().getColumn(5).setPreferredWidth(100);
    }
        
    private void LoadPustakawan () {
        
        String sql = "SELECT * FROM pustakawan ORDER BY id ASC";
        
        try {
            PreparedStatement st = DBConnection.getConnection().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = st.executeQuery();
            
            //menambahkan kolom pada header
            String[]header = {"No.","ID","Username", "Nama Lengkap", "Nomor HP", "Alamat Email"};
            
            rs.last();
            int baris = rs.getRow();
            rs.beforeFirst();
            
            Object[][] dttbl = new Object[baris][6]; // Jumlah kolom ditambah 1
            int curbaris = 0;
            int nomor = 1; // Variabel nomor untuk penomoran
            while (rs.next()) {
                dttbl[curbaris][0] = nomor; // Menyimpan nilai nomor pada kolom pertama
                dttbl[curbaris][1] = rs.getString("id");
                dttbl[curbaris][2] = rs.getString("username");
                dttbl[curbaris][3] = rs.getString("nama_lengkap");
                dttbl[curbaris][4] = rs.getString("nomor_hp");
                dttbl[curbaris][5] = rs.getString("email");
                curbaris++;
                nomor++; // Menaikkan nilai nomor setiap iterasi
            }
            TablePustakawan.setModel(new DefaultTableModel(dttbl, header));
            setColumnWidth();
               
        }catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            System.out.println(e);
        }
    }
    
    
    private void TambahPustakawan() {
        
        String sqlInsert = "INSERT INTO pustakawan (id, username, password, nama_lengkap, nomor_hp, email) VALUES (NULL, ?, ?, ?, ?, ?)";

        try {
            PreparedStatement stInsert = DBConnection.getConnection().prepareStatement(sqlInsert);

            stInsert.setString(1, TxtUsername.getText());

            String password = new String(TxtPassword.getPassword());
            String hashedPassword = getMD5Hash(password); // Meng-hash password menggunakan MD5

            stInsert.setString(2, hashedPassword);
            stInsert.setString(3, TxtNamaLengkap.getText());
            stInsert.setString(4, TxtNomorHandphone.getText());
            stInsert.setString(5, TxtEmail.getText());

            stInsert.executeUpdate();

            JOptionPane.showMessageDialog(null, "Data Berhasil Disimpan.");

            // Mengosongkan JTextField
            TxtUsername.setText("");
            TxtPassword.setText("");
            TxtNamaLengkap.setText("");
            TxtNomorHandphone.setText("");
            TxtEmail.setText("");

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            System.out.println(e);
        }
    }
    
    private void EditPustakawan(Object id) {
        String sqlWithoutPassword = "UPDATE pustakawan SET username=?, nama_lengkap=?, nomor_hp=?, email=? WHERE id=" + id;
        String sqlWithPassword = "UPDATE pustakawan SET username=?, password=?, nama_lengkap=?, nomor_hp=?, email=? WHERE id=" + id;

        try {
            String password = new String(TxtPassword.getPassword());

            if (!password.isEmpty()) { // Jika password tidak kosong
                PreparedStatement stInsert = DBConnection.getConnection().prepareStatement(sqlWithPassword);
                stInsert.setString(1, TxtUsername.getText());
                stInsert.setString(2, getMD5Hash(password));
                stInsert.setString(3, TxtNamaLengkap.getText());
                stInsert.setString(4, TxtNomorHandphone.getText());
                stInsert.setString(5, TxtEmail.getText());
                stInsert.executeUpdate();
            } else { // Jika password kosong
                PreparedStatement stInsert = DBConnection.getConnection().prepareStatement(sqlWithoutPassword);
                stInsert.setString(1, TxtUsername.getText());
                stInsert.setString(2, TxtNamaLengkap.getText());
                stInsert.setString(3, TxtNomorHandphone.getText());
                stInsert.setString(4, TxtEmail.getText());
                stInsert.executeUpdate();
            }

            JOptionPane.showMessageDialog(null, "Data Berhasil Dirubah.");

            // Mengosongkan JTextField
            TxtUsername.setText("");
            TxtPassword.setText("");
            TxtNamaLengkap.setText("");
            TxtNomorHandphone.setText("");
            TxtEmail.setText("");

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            System.out.println(e);
        }
    }


    
    private void HapusPustakawan(Object id) {
        
        String sql = "DELETE FROM pustakawan WHERE id='"+id+"'";
    
        try {
            PreparedStatement st = DBConnection.getConnection().prepareStatement(sql);

            st.executeUpdate();
            JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus.");

            // Mengosongkan JTextField
            TxtNamaLengkap.setText("");
            TxtNomorHandphone.setText("");
            TxtEmail.setText("");
            TxtUsername.setText("");
            TxtPassword.setText("");

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            System.out.println(e);

        }
    }
    
    
    private String getMD5Hash(String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(password.getBytes());

            StringBuilder sb = new StringBuilder();
            for (byte b : messageDigest) {
                sb.append(String.format("%02x", b));
            }

            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        headerInside1 = new pbo.unsia.le.brarian.HeaderInside();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TablePustakawan = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        TxtNamaLengkap = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        TxtNomorHandphone = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        TxtEmail = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        BtnHapusPustakawan = new javax.swing.JButton();
        BtnTambahPustakawan = new javax.swing.JButton();
        BtnEditPustakawan = new javax.swing.JButton();
        TxtUsername = new javax.swing.JTextField();
        TxtPassword = new javax.swing.JPasswordField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        headerInside1.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Helvetica Neue", 1, 36)); // NOI18N
        jLabel1.setText("Data Pustakawan");
        headerInside1.add(jLabel1);
        jLabel1.setBounds(350, 10, 320, 50);

        jPanel1.setBackground(new java.awt.Color(38, 40, 22));
        jPanel1.setForeground(new java.awt.Color(255, 255, 255));
        jPanel1.setToolTipText("");
        jPanel1.setLayout(null);

        TablePustakawan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        TablePustakawan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TablePustakawanMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(TablePustakawan);

        jPanel1.add(jScrollPane1);
        jScrollPane1.setBounds(30, 10, 470, 330);

        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Nama Lengkap");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(520, 10, 250, 16);
        jPanel1.add(TxtNamaLengkap);
        TxtNamaLengkap.setBounds(520, 30, 250, 30);

        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Nomor Handphone");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(520, 80, 250, 16);
        jPanel1.add(TxtNomorHandphone);
        TxtNomorHandphone.setBounds(520, 100, 250, 30);

        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Email");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(520, 150, 250, 16);
        jPanel1.add(TxtEmail);
        TxtEmail.setBounds(520, 170, 250, 30);

        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Username");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(520, 220, 250, 16);

        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Password");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(520, 290, 250, 16);

        BtnHapusPustakawan.setText("Hapus Pustakawan");
        BtnHapusPustakawan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHapusPustakawanActionPerformed(evt);
            }
        });
        jPanel1.add(BtnHapusPustakawan);
        BtnHapusPustakawan.setBounds(800, 130, 170, 30);

        BtnTambahPustakawan.setText("Tambah Pustakawan");
        BtnTambahPustakawan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnTambahPustakawanActionPerformed(evt);
            }
        });
        jPanel1.add(BtnTambahPustakawan);
        BtnTambahPustakawan.setBounds(800, 30, 170, 30);

        BtnEditPustakawan.setText("Edit Pustakawan");
        BtnEditPustakawan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditPustakawanActionPerformed(evt);
            }
        });
        jPanel1.add(BtnEditPustakawan);
        BtnEditPustakawan.setBounds(800, 80, 170, 30);
        jPanel1.add(TxtUsername);
        TxtUsername.setBounds(520, 240, 250, 30);
        jPanel1.add(TxtPassword);
        TxtPassword.setBounds(520, 310, 250, 30);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(headerInside1, javax.swing.GroupLayout.PREFERRED_SIZE, 1000, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1000, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(headerInside1, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void BtnHapusPustakawanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHapusPustakawanActionPerformed
        // TODO add your handling code here:
        if(globalPustakawanId != null) {
            HapusPustakawan(globalPustakawanId);
            this.requestFocus();
            LoadPustakawan();
        }
        else {
            JOptionPane.showMessageDialog(null, "Pilih data dari tabel terlebih dahulu!");
        }
    }//GEN-LAST:event_BtnHapusPustakawanActionPerformed

    private void BtnTambahPustakawanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnTambahPustakawanActionPerformed
        // TODO add your handling code here:
        TambahPustakawan();
        this.requestFocus();
        LoadPustakawan();
    }//GEN-LAST:event_BtnTambahPustakawanActionPerformed

    private void BtnEditPustakawanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditPustakawanActionPerformed
        // TODO add your handling code here:
        if(globalPustakawanId != null) {
            EditPustakawan(globalPustakawanId);
            this.requestFocus();
            LoadPustakawan();
        }
        else {
            JOptionPane.showMessageDialog(null, "Pilih data dari tabel terlebih dahulu!");
        }
    }//GEN-LAST:event_BtnEditPustakawanActionPerformed

    private void TablePustakawanMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TablePustakawanMouseClicked
        // TODO add your handling code here:
        int selectedRow = TablePustakawan.getSelectedRow();
        
        globalPustakawanId = TablePustakawan.getValueAt(selectedRow, 1);
        
        Object username = TablePustakawan.getValueAt(selectedRow, 2);
        Object nama_lengkap = TablePustakawan.getValueAt(selectedRow, 3);
        Object nomor_hp = TablePustakawan.getValueAt(selectedRow, 4);
        Object email = TablePustakawan.getValueAt(selectedRow, 5);
        
        TxtNamaLengkap.setText(nama_lengkap.toString());
        TxtNomorHandphone.setText(nomor_hp.toString());
        TxtEmail.setText(email.toString());
        TxtUsername.setText(username.toString());
    }//GEN-LAST:event_TablePustakawanMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DataPustakawan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DataPustakawan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DataPustakawan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DataPustakawan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new DataPustakawan().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnEditPustakawan;
    private javax.swing.JButton BtnHapusPustakawan;
    private javax.swing.JButton BtnTambahPustakawan;
    private javax.swing.JTable TablePustakawan;
    private javax.swing.JTextField TxtEmail;
    private javax.swing.JTextField TxtNamaLengkap;
    private javax.swing.JTextField TxtNomorHandphone;
    private javax.swing.JPasswordField TxtPassword;
    private javax.swing.JTextField TxtUsername;
    private pbo.unsia.le.brarian.HeaderInside headerInside1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
