/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package pbo.unsia.le.brarian;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author syarifmaulana
 */
public class RiwayatPengembalian extends javax.swing.JFrame {

    /**
     * Creates new form RiwayatPengembalian
     */
    public RiwayatPengembalian() {
        initComponents();
        DBConnection.openConnection();
        LoadData();
        this.setDefaultCloseOperation(PeminjamanBuku.DISPOSE_ON_CLOSE);
    }
    
    
    private void setColumnWidth() {
        TablePengembalian.getColumnModel().getColumn(0).setPreferredWidth(30);
        TablePengembalian.getColumnModel().getColumn(1).setPreferredWidth(200);
        TablePengembalian.getColumnModel().getColumn(2).setPreferredWidth(200);
        TablePengembalian.getColumnModel().getColumn(3).setPreferredWidth(150);
        TablePengembalian.getColumnModel().getColumn(4).setPreferredWidth(150);
        TablePengembalian.getColumnModel().getColumn(5).setPreferredWidth(100);
        TablePengembalian.getColumnModel().getColumn(6).setPreferredWidth(100);
        TablePengembalian.getColumnModel().getColumn(7).setPreferredWidth(100);
    }
    
    private void LoadData() {
        String sql = "SELECT * FROM peminjaman WHERE status='1' ORDER BY id ASC";

        try {
            PreparedStatement st = DBConnection.getConnection().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = st.executeQuery();

            // Menambahkan kolom nomor pada header
            String[] header = {"No.", "No. ISBN", "Judul Buku", "Nama Peminjam", "Tgl. Peminjaman", "Tgl. Pengembalian", "Tgl. Dikembalikan", "Status"};

            rs.last();
            int baris = rs.getRow();
            rs.beforeFirst();

            Object[][] dttbl = new Object[baris][8]; // Jumlah kolom ditambah menjadi 8
            int curbaris = 0;
            int nomor = 1; // Variabel nomor untuk penomoran
            while (rs.next()) {
                dttbl[curbaris][0] = nomor;
                dttbl[curbaris][1] = rs.getString("nomor_isbn");
                dttbl[curbaris][2] = rs.getString("judul_buku");
                dttbl[curbaris][3] = rs.getString("nama_peminjam");
                dttbl[curbaris][4] = rs.getString("tanggal_peminjaman");
                dttbl[curbaris][5] = rs.getString("tanggal_pengembalian");
                dttbl[curbaris][6] = rs.getString("tanggal_dikembalikan");

                String tanggalDikembalikan = rs.getString("tanggal_dikembalikan");
                String tanggalPengembalian = rs.getString("tanggal_pengembalian");

                // Cek status pengembalian
                if (tanggalDikembalikan != null && tanggalPengembalian != null) {
                    try {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        Date dateDikembalikan = dateFormat.parse(tanggalDikembalikan);
                        Date datePengembalian = dateFormat.parse(tanggalPengembalian);

                        if (dateDikembalikan.after(datePengembalian)) {
                            dttbl[curbaris][7] = "Terlambat";
                        } else {
                            dttbl[curbaris][7] = "Tepat Waktu";
                        }
                    } catch (ParseException e) {
                        dttbl[curbaris][7] = "Belum Dikembalikan";
                    }
                } else {
                    dttbl[curbaris][7] = "Belum Dikembalikan";
                }

                curbaris++;
                nomor++;
            }
            TablePengembalian.setModel(new DefaultTableModel(dttbl, header));
            setColumnWidth();

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            System.out.println(e);
        }
    }
    
    
    private void CariDataPengembalian(String entry) {
        String sql = "SELECT * FROM peminjaman WHERE status='1' AND (nomor_isbn LIKE '%" +entry+ "%' OR judul_buku LIKE '%" +entry+ "%' OR nama_peminjam LIKE '%" +entry+ "%') ORDER BY id ASC";

        try {
            PreparedStatement st = DBConnection.getConnection().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = st.executeQuery();

            // Menambahkan kolom nomor pada header
            String[] header = {"No.", "No. ISBN", "Judul Buku", "Nama Peminjam", "Tgl. Peminjaman", "Tgl. Pengembalian", "Tgl. Dikembalikan", "Status"};

            rs.last();
            int baris = rs.getRow();
            rs.beforeFirst();

            Object[][] dttbl = new Object[baris][8]; // Jumlah kolom ditambah menjadi 8
            int curbaris = 0;
            int nomor = 1; // Variabel nomor untuk penomoran
            while (rs.next()) {
                dttbl[curbaris][0] = nomor;
                dttbl[curbaris][1] = rs.getString("nomor_isbn");
                dttbl[curbaris][2] = rs.getString("judul_buku");
                dttbl[curbaris][3] = rs.getString("nama_peminjam");
                dttbl[curbaris][4] = rs.getString("tanggal_peminjaman");
                dttbl[curbaris][5] = rs.getString("tanggal_pengembalian");
                dttbl[curbaris][6] = rs.getString("tanggal_dikembalikan");

                String tanggalDikembalikan = rs.getString("tanggal_dikembalikan");
                String tanggalPengembalian = rs.getString("tanggal_pengembalian");

                // Cek status pengembalian
                if (tanggalDikembalikan != null && tanggalPengembalian != null) {
                    try {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        Date dateDikembalikan = dateFormat.parse(tanggalDikembalikan);
                        Date datePengembalian = dateFormat.parse(tanggalPengembalian);

                        if (dateDikembalikan.after(datePengembalian)) {
                            dttbl[curbaris][7] = "Terlambat";
                        } else {
                            dttbl[curbaris][7] = "Tepat Waktu";
                        }
                    } catch (ParseException e) {
                        dttbl[curbaris][7] = "Belum Dikembalikan";
                    }
                } else {
                    dttbl[curbaris][7] = "Belum Dikembalikan";
                }

                curbaris++;
                nomor++;
            }
            TablePengembalian.setModel(new DefaultTableModel(dttbl, header));
            setColumnWidth();

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            System.out.println(e);
        }
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        headerInside1 = new pbo.unsia.le.brarian.HeaderInside();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TablePengembalian = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        TxtCariPengembalian = new javax.swing.JTextField();
        BtnCariPengembalian = new javax.swing.JButton();
        BtnPrint = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        headerInside1.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Helvetica Neue", 1, 36)); // NOI18N
        jLabel1.setText("Riwayat Pengembalian");
        headerInside1.add(jLabel1);
        jLabel1.setBounds(300, 10, 410, 50);

        jPanel1.setBackground(new java.awt.Color(38, 40, 22));
        jPanel1.setForeground(new java.awt.Color(255, 255, 255));
        jPanel1.setToolTipText("");
        jPanel1.setLayout(null);

        TablePengembalian.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(TablePengembalian);

        jPanel1.add(jScrollPane1);
        jScrollPane1.setBounds(20, 90, 960, 290);

        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Cari Data");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(20, 20, 250, 16);
        jPanel1.add(TxtCariPengembalian);
        TxtCariPengembalian.setBounds(20, 40, 250, 30);

        BtnCariPengembalian.setText("Cari");
        BtnCariPengembalian.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariPengembalianActionPerformed(evt);
            }
        });
        jPanel1.add(BtnCariPengembalian);
        BtnCariPengembalian.setBounds(290, 40, 150, 30);

        BtnPrint.setText("Print");
        BtnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPrintActionPerformed(evt);
            }
        });
        jPanel1.add(BtnPrint);
        BtnPrint.setBounds(830, 40, 150, 30);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(headerInside1, javax.swing.GroupLayout.PREFERRED_SIZE, 1000, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1000, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(headerInside1, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void BtnCariPengembalianActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariPengembalianActionPerformed
        // TODO add your handling code here:
        String entry = TxtCariPengembalian.getText();
        CariDataPengembalian(entry);
        this.requestFocus();
    }//GEN-LAST:event_BtnCariPengembalianActionPerformed

    private void BtnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPrintActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnPrintActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RiwayatPengembalian.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RiwayatPengembalian.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RiwayatPengembalian.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RiwayatPengembalian.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RiwayatPengembalian().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnCariPengembalian;
    private javax.swing.JButton BtnPrint;
    private javax.swing.JTable TablePengembalian;
    private javax.swing.JTextField TxtCariPengembalian;
    private pbo.unsia.le.brarian.HeaderInside headerInside1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
