/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package pbo.unsia.le.brarian;

/**
 *
 * @author syarifmaulana
 */

import java.sql.Connection;
import javax.swing.JComboBox;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class PeminjamanBuku extends javax.swing.JFrame {

    /**
     * Creates new form PeminjamanBuku
     */
    
    public PeminjamanBuku() {
        initComponents();
        DBConnection.openConnection();
        LoadAnggotaData();
        LoadBuku();
        TxtNomorISBN.setEnabled(false);
        TxtJudulBuku.setEnabled(false);
        this.setDefaultCloseOperation(PeminjamanBuku.DISPOSE_ON_CLOSE);
    }
    
    
    private void setColumnWidth() {
        TableBuku.getColumnModel().getColumn(0).setPreferredWidth(30);
        TableBuku.getColumnModel().getColumn(1).setPreferredWidth(50);
        TableBuku.getColumnModel().getColumn(2).setPreferredWidth(100);
        TableBuku.getColumnModel().getColumn(3).setPreferredWidth(100);
        TableBuku.getColumnModel().getColumn(4).setPreferredWidth(50);
        TableBuku.getColumnModel().getColumn(5).setPreferredWidth(50);
    }    
    
    private void LoadBuku() {
        
        String sql = "SELECT * FROM buku WHERE jumlah_buku > 0 ORDER BY id ASC";

        try {
            PreparedStatement st = DBConnection.getConnection().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = st.executeQuery();

            // Menambahkan kolom nomor pada header
            String[] header = {"No.", "No. ISBN", "Judul Buku", "Nama Penulis", "Tahun Terbit", "Jumlah"};

            rs.last();
            int baris = rs.getRow();
            rs.beforeFirst();

            Object[][] dttbl = new Object[baris][6]; // Jumlah kolom ditambah 1
            int curbaris = 0;
            int nomor = 1; // Variabel nomor untuk penomoran
            while (rs.next()) {
                dttbl[curbaris][0] = nomor; // Menyimpan nilai nomor pada kolom pertama
                dttbl[curbaris][1] = rs.getString("nomor_isbn");
                dttbl[curbaris][2] = rs.getString("judul_buku");
                dttbl[curbaris][3] = rs.getString("nama_penulis");
                dttbl[curbaris][4] = rs.getString("tahun_terbit");
                dttbl[curbaris][5] = rs.getString("jumlah_buku");
                curbaris++;
                nomor++; // Menaikkan nilai nomor setiap iterasi
            }
            TableBuku.setModel(new DefaultTableModel(dttbl, header));
            setColumnWidth();

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            System.out.println(e);
        }
    }
    
    
    private void SearchBuku(String entry) {
        
        String sql = "SELECT * FROM buku WHERE (nomor_isbn LIKE '%" + entry + "%' OR judul_buku LIKE '%" + entry + "%' OR nama_penulis LIKE '%" + entry + "%') AND jumlah_buku > 0;";

        try {
            PreparedStatement st = DBConnection.getConnection().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = st.executeQuery();

            // Menambahkan kolom nomor pada header
            String[] header = {"No.", "No. ISBN", "Judul Buku", "Nama Penulis", "Tahun Terbit", "Jumlah"};

            rs.last();
            int baris = rs.getRow();
            rs.beforeFirst();

            Object[][] dttbl = new Object[baris][6]; // Jumlah kolom ditambah 1
            int curbaris = 0;
            int nomor = 1; // Variabel nomor untuk penomoran
            while (rs.next()) {
                dttbl[curbaris][0] = nomor; // Menyimpan nilai nomor pada kolom pertama
                dttbl[curbaris][1] = rs.getString("nomor_isbn");
                dttbl[curbaris][2] = rs.getString("judul_buku");
                dttbl[curbaris][3] = rs.getString("nama_penulis");
                dttbl[curbaris][4] = rs.getString("tahun_terbit");
                dttbl[curbaris][5] = rs.getString("jumlah_buku");
                curbaris++;
                nomor++; // Menaikkan nilai nomor setiap iterasi
            }
            TableBuku.setModel(new DefaultTableModel(dttbl, header));
            setColumnWidth();

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            System.out.println(e);
        }
    }
    private void LoadAnggotaData() {
        
        String sql = "SELECT nama_lengkap FROM anggota ORDER BY id ASC";
        JComboBox<String> comboBox = NamaPeminjam;

        try {
            PreparedStatement st = DBConnection.getConnection().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = st.executeQuery();
            
            comboBox.removeAllItems();
            
            while (rs.next()) {
                
                String namaAnggota = rs.getString("nama_lengkap");
                comboBox.addItem(namaAnggota);
            }

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            System.out.println(e);
        }
    }
    
    private void TambahPeminjaman() {
        String sqlInsert = "INSERT INTO peminjaman VALUES(NULL, ?, ?, ?, ?, ?,NULL, 0)";
        String sqlUpdate = "UPDATE buku SET jumlah_buku = ? WHERE nomor_isbn = ?";

        try {
            Connection connection = DBConnection.getConnection();
            PreparedStatement stInsert = connection.prepareStatement(sqlInsert);
            PreparedStatement stUpdate = connection.prepareStatement(sqlUpdate);

            stInsert.setString(1, TxtNomorISBN.getText());
            stInsert.setString(2, NamaPeminjam.getSelectedItem().toString());
            stInsert.setString(3, TxtJudulBuku.getText());

            Date tanggalPeminjaman = TanggalPeminjaman.getDate();
            java.sql.Date tanggal_peminjaman = new java.sql.Date(tanggalPeminjaman.getTime());
            stInsert.setDate(4, tanggal_peminjaman);

            Date tanggalPengembalian = TanggalPemgembalian.getDate();
            java.sql.Date tanggal_pengembalian = new java.sql.Date(tanggalPengembalian.getTime());
            stInsert.setDate(5, tanggal_pengembalian);

            // Execute the INSERT statement
            stInsert.executeUpdate();

            // Update the value of jumlah_buku in the buku table
            int updatedJumlahBuku = getUpdatedJumlahBuku(connection, TxtNomorISBN.getText());
            stUpdate.setInt(1, updatedJumlahBuku);
            stUpdate.setString(2, TxtNomorISBN.getText());
            stUpdate.executeUpdate();

            JOptionPane.showMessageDialog(null, "Data Peminjaman Berhasil Ditambah.");

            // Mengosongkan JTextField
            TxtNomorISBN.setText("");
            TxtJudulBuku.setText("");
            TanggalPeminjaman.setDate(null);
            TanggalPemgembalian.setDate(null);

            LoadBuku();

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            System.out.println(e);
        }
    }

    private int getUpdatedJumlahBuku(Connection connection, String nomorIsbn) throws SQLException {
        String sqlSelect = "SELECT jumlah_buku FROM buku WHERE nomor_isbn = ?";
        PreparedStatement stSelect = connection.prepareStatement(sqlSelect);
        stSelect.setString(1, nomorIsbn);
        ResultSet rs = stSelect.executeQuery();
        int jumlahBuku = 0;
        if (rs.next()) {
            jumlahBuku = rs.getInt("jumlah_buku");
        }
        return jumlahBuku - 1;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        NamaPeminjam = new javax.swing.JComboBox<>();
        TanggalPemgembalian = new com.toedter.calendar.JDateChooser();
        TanggalPeminjaman = new com.toedter.calendar.JDateChooser();
        BtnSimpan1 = new javax.swing.JButton();
        TxtJudulBuku = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        TableBuku = new javax.swing.JTable();
        BtnCariBuku = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        TxtCariBuku = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        TxtNomorISBN = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        headerInside1 = new pbo.unsia.le.brarian.HeaderInside();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Peminjaman Buku");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(38, 40, 22));
        jPanel1.setLayout(null);

        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel1.setText("Tanggal Pengembalian");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(680, 230, 150, 16);

        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel5.setText("Tanggal Peminjaman");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(520, 230, 150, 16);

        NamaPeminjam.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        NamaPeminjam.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NamaPeminjamActionPerformed(evt);
            }
        });
        jPanel1.add(NamaPeminjam);
        NamaPeminjam.setBounds(520, 180, 310, 30);
        jPanel1.add(TanggalPemgembalian);
        TanggalPemgembalian.setBounds(680, 250, 150, 30);
        jPanel1.add(TanggalPeminjaman);
        TanggalPeminjaman.setBounds(520, 250, 150, 30);

        BtnSimpan1.setBackground(new java.awt.Color(255, 204, 0));
        BtnSimpan1.setText("Tambah Data");
        BtnSimpan1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSimpan1ActionPerformed(evt);
            }
        });
        jPanel1.add(BtnSimpan1);
        BtnSimpan1.setBounds(680, 300, 150, 30);
        jPanel1.add(TxtJudulBuku);
        TxtJudulBuku.setBounds(520, 120, 310, 30);

        TableBuku.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        TableBuku.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TableBukuMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(TableBuku);

        jPanel1.add(jScrollPane1);
        jScrollPane1.setBounds(20, 60, 480, 270);

        BtnCariBuku.setText("Cari Buku");
        BtnCariBuku.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariBukuActionPerformed(evt);
            }
        });
        jPanel1.add(BtnCariBuku);
        BtnCariBuku.setBounds(410, 20, 90, 30);

        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel4.setText("Nama Anggota");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(520, 160, 120, 16);
        jPanel1.add(TxtCariBuku);
        TxtCariBuku.setBounds(21, 20, 370, 30);

        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel6.setText("Judul Buku");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(520, 100, 120, 16);
        jPanel1.add(TxtNomorISBN);
        TxtNomorISBN.setBounds(520, 50, 310, 30);

        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel7.setText("Nomor ISBN");
        jPanel1.add(jLabel7);
        jLabel7.setBounds(520, 20, 120, 16);

        headerInside1.setPreferredSize(new java.awt.Dimension(860, 150));

        jLabel2.setFont(new java.awt.Font("Helvetica Neue", 1, 36)); // NOI18N
        jLabel2.setText("Data Peminjaman");

        javax.swing.GroupLayout headerInside1Layout = new javax.swing.GroupLayout(headerInside1);
        headerInside1.setLayout(headerInside1Layout);
        headerInside1Layout.setHorizontalGroup(
            headerInside1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, headerInside1Layout.createSequentialGroup()
                .addContainerGap(292, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addGap(269, 269, 269))
        );
        headerInside1Layout.setVerticalGroup(
            headerInside1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(headerInside1Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(84, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(headerInside1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(headerInside1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 359, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void NamaPeminjamActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NamaPeminjamActionPerformed
        // TODO add your handling code here:   
    }//GEN-LAST:event_NamaPeminjamActionPerformed

    private void BtnSimpan1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSimpan1ActionPerformed
        // TODO add your handling code here:
        TambahPeminjaman();
    }//GEN-LAST:event_BtnSimpan1ActionPerformed

    private void BtnCariBukuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariBukuActionPerformed
        // TODO add your handling code here:
        String entry = TxtCariBuku.getText();
        SearchBuku(entry);
        this.requestFocus();
    }//GEN-LAST:event_BtnCariBukuActionPerformed

    private void TableBukuMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TableBukuMouseClicked
        // TODO add your handling code here:
        int selectedRow = TableBuku.getSelectedRow();
              
        Object nomor_isbn = TableBuku.getValueAt(selectedRow, 1);
        Object judul_buku = TableBuku.getValueAt(selectedRow, 2);
        TxtJudulBuku.setText(judul_buku.toString());
        TxtNomorISBN.setText(nomor_isbn.toString());

    }//GEN-LAST:event_TableBukuMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PeminjamanBuku.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PeminjamanBuku.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PeminjamanBuku.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PeminjamanBuku.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PeminjamanBuku().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnCariBuku;
    private javax.swing.JButton BtnSimpan1;
    private javax.swing.JComboBox<String> NamaPeminjam;
    private javax.swing.JTable TableBuku;
    private com.toedter.calendar.JDateChooser TanggalPemgembalian;
    private com.toedter.calendar.JDateChooser TanggalPeminjaman;
    private javax.swing.JTextField TxtCariBuku;
    private javax.swing.JTextField TxtJudulBuku;
    private javax.swing.JTextField TxtNomorISBN;
    private pbo.unsia.le.brarian.HeaderInside headerInside1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
