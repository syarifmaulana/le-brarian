/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package pbo.unsia.le.brarian;

/**
 *
 * @author syarifmaulana
 */

import javax.swing.JOptionPane;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import javax.swing.table.DefaultTableModel;

public class DataAnggota extends javax.swing.JFrame {

    /**
     * Creates new form DataAnggota
     */
    
    private Object globalAnggotaId;
    
    public DataAnggota() {
        initComponents();
        DBConnection.openConnection();
        LoadAnggota();
        this.setDefaultCloseOperation(PeminjamanBuku.DISPOSE_ON_CLOSE);
    }
    
    private void setColumnWidth() {
        TableAnggota.getColumnModel().getColumn(0).setPreferredWidth(20);
        TableAnggota.getColumnModel().getColumn(1).setPreferredWidth(20);
        TableAnggota.getColumnModel().getColumn(3).setPreferredWidth(100);
        TableAnggota.getColumnModel().getColumn(5).setPreferredWidth(100);
    }
    
    private void LoadAnggota() {
        
        String sql = "SELECT * FROM anggota ORDER BY id ASC";

        try {
            PreparedStatement st = DBConnection.getConnection().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = st.executeQuery();

            // Menambahkan kolom nomor pada header
            String[] header = {"No.", "Kode", "Nama", "Nomor HP", "Alamat Email", "Tanggal Bergabung"};

            rs.last();
            int baris = rs.getRow();
            rs.beforeFirst();

            Object[][] dttbl = new Object[baris][6]; // Jumlah kolom ditambah 1
            int curbaris = 0;
            int nomor = 1; // Variabel nomor untuk penomoran
            while (rs.next()) {
                dttbl[curbaris][0] = nomor; // Menyimpan nilai nomor pada kolom pertama
                dttbl[curbaris][1] = rs.getString("kode_anggota");
                dttbl[curbaris][2] = rs.getString("nama_lengkap");
                dttbl[curbaris][3] = rs.getString("nomor_hp");
                dttbl[curbaris][4] = rs.getString("email");
                dttbl[curbaris][5] = rs.getString("tanggal_bergabung");
                curbaris++;
                nomor++; // Menaikkan nilai nomor setiap iterasi
            }
            TableAnggota.setModel(new DefaultTableModel(dttbl, header));
            setColumnWidth();

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            System.out.println(e);
        }
    }

    
    private void SearchAnggota(String entry) {
        
        String sql = "SELECT * FROM anggota WHERE nama_lengkap LIKE '%" + entry + "%' OR email LIKE '%" + entry + "%';";

        try {
            PreparedStatement st = DBConnection.getConnection().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = st.executeQuery();

            /// Menambahkan kolom nomor pada header
            String[] header = {"No.", "Kode", "Nama", "Nomor HP", "Alamat Email", "Tanggal Bergabung"};

            rs.last();
            int baris = rs.getRow();
            rs.beforeFirst();

            Object[][] dttbl = new Object[baris][6]; // Jumlah kolom ditambah 1
            int curbaris = 0;
            int nomor = 1; // Variabel nomor untuk penomoran
            while (rs.next()) {
                dttbl[curbaris][0] = nomor; // Menyimpan nilai nomor pada kolom pertama
                dttbl[curbaris][1] = rs.getString("kode_anggota");
                dttbl[curbaris][2] = rs.getString("nama_lengkap");
                dttbl[curbaris][3] = rs.getString("nomor_hp");
                dttbl[curbaris][4] = rs.getString("email");
                dttbl[curbaris][5] = rs.getString("tanggal_bergabung");
                curbaris++;
                nomor++; // Menaikkan nilai nomor setiap iterasi
            }
            TableAnggota.setModel(new DefaultTableModel(dttbl, header));
            setColumnWidth();

        } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e);
                System.out.println(e);
        }
    }
    
    
    private void TambahAnggota() {
        String sqlInsert = "INSERT INTO anggota (id, kode_anggota, nama_lengkap, nomor_hp, email, tanggal_bergabung) VALUES (NULL, ?, ?, ?, ?, ?)";
        String sqlSelect = "SELECT MAX(id) AS max_id FROM anggota";

        try {

            PreparedStatement stSelect = DBConnection.getConnection().prepareStatement(sqlSelect);
            ResultSet rs = stSelect.executeQuery();

            int maxId = 0;
            if (rs.next()) {
                maxId = rs.getInt("max_id");
            }

            String kodeAnggota = "LIB-" + (maxId + 1);

            PreparedStatement stInsert = DBConnection.getConnection().prepareStatement(sqlInsert);
            stInsert.setString(1, kodeAnggota);
            stInsert.setString(2, TxtNamaLengkap.getText());
            stInsert.setString(3, TxtNomorHandphone.getText());
            stInsert.setString(4, TxtEmail.getText());

            // Mengambil tanggal hari ini
            Date date = new Date();
            Date tanggalSekarang = new Date(date.getTime());
            stInsert.setDate(5, new java.sql.Date(tanggalSekarang.getTime()));

            stInsert.executeUpdate();

            JOptionPane.showMessageDialog(null, "Data Berhasil Disimpan.");

            // Mengosongkan JTextField
            TxtNamaLengkap.setText("");
            TxtNomorHandphone.setText("");
            TxtEmail.setText("");

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            System.out.println(e);
        }
    }
    
    private void EditAnggota(Object id) {
        
        String sql = "UPDATE anggota SET nama_lengkap=?, nomor_hp=?, email=? WHERE kode_anggota='"+id+"'";
    
        try {
            PreparedStatement st = DBConnection.getConnection().prepareStatement(sql);

            st.setString(1, TxtNamaLengkap.getText());
            st.setString(2, TxtNomorHandphone.getText());
            st.setString(3, TxtEmail.getText());

            st.executeUpdate();
            JOptionPane.showMessageDialog(null, "Data Berhasil Dirubah.");

            // Mengosongkan JTextField
            TxtNamaLengkap.setText("");
            TxtNomorHandphone.setText("");
            TxtEmail.setText("");

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            System.out.println(e);

        }
    }
    
    private void HapusAnggota(Object id) {
        
        String sql = "DELETE FROM anggota WHERE kode_anggota='"+id+"'";
    
        try {
            PreparedStatement st = DBConnection.getConnection().prepareStatement(sql);

            st.executeUpdate();
            JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus.");

            // Mengosongkan JTextField
            TxtNamaLengkap.setText("");
            TxtNomorHandphone.setText("");
            TxtEmail.setText("");

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            System.out.println(e);

        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        headerInside1 = new pbo.unsia.le.brarian.HeaderInside();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TableAnggota = new javax.swing.JTable();
        TxtCariAnggota = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        TxtNamaLengkap = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        TxtNomorHandphone = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        BtnHapusAnggota = new javax.swing.JButton();
        BtnCariAnggota = new javax.swing.JButton();
        BtnEditAnggota = new javax.swing.JButton();
        TxtEmail = new javax.swing.JTextField();
        BtnTambahAnggota = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Data Anggota");
        setResizable(false);

        headerInside1.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Helvetica Neue", 1, 36)); // NOI18N
        jLabel1.setText("Data Anggota");
        headerInside1.add(jLabel1);
        jLabel1.setBounds(380, 10, 250, 50);

        jPanel1.setBackground(new java.awt.Color(38, 40, 22));
        jPanel1.setForeground(new java.awt.Color(255, 255, 255));
        jPanel1.setToolTipText("");
        jPanel1.setLayout(null);

        TableAnggota.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        TableAnggota.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TableAnggotaMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(TableAnggota);

        jPanel1.add(jScrollPane1);
        jScrollPane1.setBounds(30, 50, 610, 280);
        jPanel1.add(TxtCariAnggota);
        TxtCariAnggota.setBounds(30, 10, 510, 30);

        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Nama Lengkap");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(660, 10, 250, 16);
        jPanel1.add(TxtNamaLengkap);
        TxtNamaLengkap.setBounds(660, 40, 320, 30);

        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Nomor Handphone");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(660, 90, 250, 16);
        jPanel1.add(TxtNomorHandphone);
        TxtNomorHandphone.setBounds(660, 120, 320, 30);

        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Email Address");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(660, 170, 250, 16);

        BtnHapusAnggota.setText("Hapus Data");
        BtnHapusAnggota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHapusAnggotaActionPerformed(evt);
            }
        });
        jPanel1.add(BtnHapusAnggota);
        BtnHapusAnggota.setBounds(880, 260, 100, 30);

        BtnCariAnggota.setText("Cari");
        BtnCariAnggota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariAnggotaActionPerformed(evt);
            }
        });
        jPanel1.add(BtnCariAnggota);
        BtnCariAnggota.setBounds(550, 10, 90, 30);

        BtnEditAnggota.setText("Edit Data");
        BtnEditAnggota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditAnggotaActionPerformed(evt);
            }
        });
        jPanel1.add(BtnEditAnggota);
        BtnEditAnggota.setBounds(780, 260, 90, 30);
        jPanel1.add(TxtEmail);
        TxtEmail.setBounds(660, 200, 320, 30);

        BtnTambahAnggota.setText("Tambah Data");
        BtnTambahAnggota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnTambahAnggotaActionPerformed(evt);
            }
        });
        jPanel1.add(BtnTambahAnggota);
        BtnTambahAnggota.setBounds(660, 260, 110, 30);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1012, Short.MAX_VALUE)
            .addComponent(headerInside1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(headerInside1, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 352, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void BtnCariAnggotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariAnggotaActionPerformed
        // TODO add your handling code here:
        String entry = TxtCariAnggota.getText();
        SearchAnggota(entry);
        this.requestFocus();
    }//GEN-LAST:event_BtnCariAnggotaActionPerformed

    private void BtnEditAnggotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditAnggotaActionPerformed
        // TODO add your handling code here:
        if(globalAnggotaId != null) {
            EditAnggota(globalAnggotaId);
            this.requestFocus();
            LoadAnggota();
        }
        else {
            JOptionPane.showMessageDialog(null, "Pilih data dari tabel terlebih dahulu!");
        }
    }//GEN-LAST:event_BtnEditAnggotaActionPerformed

    private void BtnTambahAnggotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnTambahAnggotaActionPerformed
        // TODO add your handling code here:
        TambahAnggota();
        this.requestFocus();
        LoadAnggota();
    }//GEN-LAST:event_BtnTambahAnggotaActionPerformed

    private void BtnHapusAnggotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHapusAnggotaActionPerformed
        // TODO add your handling code here:
        if(globalAnggotaId != null) {
            HapusAnggota(globalAnggotaId);
            this.requestFocus();
            LoadAnggota();
        }
        else {
            JOptionPane.showMessageDialog(null, "Pilih data dari tabel terlebih dahulu!");
        }
    }//GEN-LAST:event_BtnHapusAnggotaActionPerformed

    private void TableAnggotaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TableAnggotaMouseClicked
        // TODO add your handling code here:
        int selectedRow = TableAnggota.getSelectedRow();
        
        globalAnggotaId = TableAnggota.getValueAt(selectedRow, 1);
        
        Object nama_anggota = TableAnggota.getValueAt(selectedRow, 2);
        Object nomor_hp = TableAnggota.getValueAt(selectedRow, 3);
        Object email = TableAnggota.getValueAt(selectedRow, 4);
        
        TxtNamaLengkap.setText(nama_anggota.toString());
        TxtNomorHandphone.setText(nomor_hp.toString());
        TxtEmail.setText(email.toString());
        
    }//GEN-LAST:event_TableAnggotaMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DataAnggota.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DataAnggota.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DataAnggota.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DataAnggota.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new DataAnggota().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnCariAnggota;
    private javax.swing.JButton BtnEditAnggota;
    private javax.swing.JButton BtnHapusAnggota;
    private javax.swing.JButton BtnTambahAnggota;
    private javax.swing.JTable TableAnggota;
    private javax.swing.JTextField TxtCariAnggota;
    private javax.swing.JTextField TxtEmail;
    private javax.swing.JTextField TxtNamaLengkap;
    private javax.swing.JTextField TxtNomorHandphone;
    private pbo.unsia.le.brarian.HeaderInside headerInside1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
