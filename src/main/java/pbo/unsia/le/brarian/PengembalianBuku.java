/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package pbo.unsia.le.brarian;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author syarifmaulana
 */
public class PengembalianBuku extends javax.swing.JFrame {

    /**
     * Creates new form PengembalianBuku
     */
    public PengembalianBuku() {
        initComponents();
        DBConnection.openConnection();
        LoadDataPeminjaman();
        this.setDefaultCloseOperation(PengembalianBuku.DISPOSE_ON_CLOSE);
    }    
    
    private void setColumnWidth() {
        TablePeminjaman.getColumnModel().getColumn(0).setPreferredWidth(30);
        TablePeminjaman.getColumnModel().getColumn(1).setPreferredWidth(100);
        TablePeminjaman.getColumnModel().getColumn(2).setPreferredWidth(100);
        TablePeminjaman.getColumnModel().getColumn(3).setPreferredWidth(100);
        TablePeminjaman.getColumnModel().getColumn(4).setPreferredWidth(50);
    }  
    
    
    private void LoadDataPeminjaman() {
        
        String sql = "SELECT * FROM peminjaman WHERE status='0' ORDER BY id ASC";

        try {
            PreparedStatement st = DBConnection.getConnection().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = st.executeQuery();

            // Menambahkan kolom nomor pada header
            String[] header = {"No.", "No. ISBN", "Nama Peminjam", "Judul Buku", "Tanggal Peminjaman"};

            rs.last();
            int baris = rs.getRow();
            rs.beforeFirst();

            Object[][] dttbl = new Object[baris][6]; // Jumlah kolom ditambah 1
            int curbaris = 0;
            int nomor = 1; // Variabel nomor untuk penomoran
            while (rs.next()) {
                dttbl[curbaris][0] = nomor; // Menyimpan nilai nomor pada kolom pertama
                dttbl[curbaris][1] = rs.getString("nomor_isbn");
                dttbl[curbaris][2] = rs.getString("nama_peminjam");
                dttbl[curbaris][3] = rs.getString("judul_buku");
                dttbl[curbaris][4] = rs.getString("tanggal_peminjaman");
                curbaris++;
                nomor++; // Menaikkan nilai nomor setiap iterasi
            }
            TablePeminjaman.setModel(new DefaultTableModel(dttbl, header));
            setColumnWidth();
            
            TxtJudulBuku.setEnabled(true);
            TxtNamaPeminjam.setEnabled(true);
            TxtNomorISBN.setEnabled(true);

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            System.out.println(e);
        }
    }
    
    
    private void CariDataPeminjaman(String entry) {
        
        String sql = "SELECT * FROM peminjaman WHERE status='0' AND (nomor_isbn LIKE '%" + entry + "%' OR nama_peminjam LIKE '%" + entry + "%' OR judul_buku LIKE '%" + entry + "%') ORDER BY id ASC";

        try {
            PreparedStatement st = DBConnection.getConnection().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = st.executeQuery();

            // Menambahkan kolom nomor pada header
            String[] header = {"No.", "No. ISBN", "Nama Peminjam", "Judul Buku", "Tanggal Peminjaman"};

            rs.last();
            int baris = rs.getRow();
            rs.beforeFirst();

            Object[][] dttbl = new Object[baris][6]; // Jumlah kolom ditambah 1
            int curbaris = 0;
            int nomor = 1; // Variabel nomor untuk penomoran
            while (rs.next()) {
                dttbl[curbaris][0] = nomor; // Menyimpan nilai nomor pada kolom pertama
                dttbl[curbaris][1] = rs.getString("nomor_isbn");
                dttbl[curbaris][2] = rs.getString("nama_peminjam");
                dttbl[curbaris][3] = rs.getString("judul_buku");
                dttbl[curbaris][4] = rs.getString("tanggal_peminjaman");
                curbaris++;
                nomor++; // Menaikkan nilai nomor setiap iterasi
            }
            TablePeminjaman.setModel(new DefaultTableModel(dttbl, header));
            setColumnWidth();
            
            TxtJudulBuku.setText("");
            TxtNamaPeminjam.setText("");
            TxtNomorISBN.setText("");
            
            TxtJudulBuku.setEnabled(true);
            TxtNamaPeminjam.setEnabled(true);
            TxtNomorISBN.setEnabled(true);

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            System.out.println(e);
        }
    }
    
    public void KembalikanBuku(String nomor_isbn, String nama_peminjam, String judul_buku, Date tanggal_pengembalian) {
        
        String sqlUpdatePeminjaman = "UPDATE peminjaman SET status='1', tanggal_dikembalikan='" +tanggal_pengembalian+ "' WHERE nomor_isbn='" +nomor_isbn+ "' AND nama_peminjam='" +nama_peminjam+ "' AND judul_buku='" +judul_buku+ "'";
        String sqlUpdateBuku = "UPDATE buku SET jumlah_buku = ? WHERE nomor_isbn = ?";
        
        try {
            Connection connection = DBConnection.getConnection();
            PreparedStatement stPeminjaman = connection.prepareStatement(sqlUpdatePeminjaman);
            PreparedStatement stBuku = connection.prepareStatement(sqlUpdateBuku);

            stPeminjaman.executeUpdate();
            
            int updatedJumlahBuku = getUpdatedJumlahBuku(connection, TxtNomorISBN.getText());
            stBuku.setInt(1, updatedJumlahBuku);
            stBuku.setString(2, TxtNomorISBN.getText());
            stBuku.executeUpdate();
            
            JOptionPane.showMessageDialog(null, "Data Pengembalian Berhasil Disimpan.");
            LoadDataPeminjaman();

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            System.out.println(e);
        }
    }
    
    private int getUpdatedJumlahBuku(Connection connection, String nomorIsbn) throws SQLException {
        String sqlSelect = "SELECT jumlah_buku FROM buku WHERE nomor_isbn = ?";
        PreparedStatement stSelect = connection.prepareStatement(sqlSelect);
        stSelect.setString(1, nomorIsbn);
        ResultSet rs = stSelect.executeQuery();
        int jumlahBuku = 0;
        if (rs.next()) {
            jumlahBuku = rs.getInt("jumlah_buku");
        }
        return jumlahBuku + 1;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        TxtCariPeminjaman = new javax.swing.JTextField();
        BtnCariPeminjaman = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        TablePeminjaman = new javax.swing.JTable();
        BtnKembalikan = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        TxtNomorISBN = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        TxtNamaPeminjam = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        TxtJudulBuku = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        TanggalPemgembalian = new com.toedter.calendar.JDateChooser();
        headerInside2 = new pbo.unsia.le.brarian.HeaderInside();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(38, 40, 22));
        jPanel1.setLayout(null);
        jPanel1.add(TxtCariPeminjaman);
        TxtCariPeminjaman.setBounds(21, 20, 330, 30);

        BtnCariPeminjaman.setText("Cari Peminjaman");
        BtnCariPeminjaman.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariPeminjamanActionPerformed(evt);
            }
        });
        jPanel1.add(BtnCariPeminjaman);
        BtnCariPeminjaman.setBounds(370, 20, 130, 30);

        TablePeminjaman.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        TablePeminjaman.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TablePeminjamanMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(TablePeminjaman);

        jPanel1.add(jScrollPane1);
        jScrollPane1.setBounds(20, 60, 480, 270);

        BtnKembalikan.setBackground(new java.awt.Color(255, 204, 0));
        BtnKembalikan.setText("Kembalikan Buku");
        BtnKembalikan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnKembalikanActionPerformed(evt);
            }
        });
        jPanel1.add(BtnKembalikan);
        BtnKembalikan.setBounds(690, 300, 140, 30);

        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel7.setText("Nomor ISBN");
        jPanel1.add(jLabel7);
        jLabel7.setBounds(520, 20, 120, 16);
        jPanel1.add(TxtNomorISBN);
        TxtNomorISBN.setBounds(520, 50, 310, 30);

        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel6.setText("Judul Buku");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(520, 100, 120, 16);
        jPanel1.add(TxtNamaPeminjam);
        TxtNamaPeminjam.setBounds(520, 180, 310, 30);

        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel4.setText("Nama Peminjam");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(520, 160, 120, 16);
        jPanel1.add(TxtJudulBuku);
        TxtJudulBuku.setBounds(520, 120, 310, 30);

        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel1.setText("Tanggal Pengembalian");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(520, 230, 150, 16);
        jPanel1.add(TanggalPemgembalian);
        TanggalPemgembalian.setBounds(520, 250, 310, 30);

        jLabel3.setFont(new java.awt.Font("Helvetica Neue", 1, 36)); // NOI18N
        jLabel3.setText("Data Pengembalian");

        javax.swing.GroupLayout headerInside2Layout = new javax.swing.GroupLayout(headerInside2);
        headerInside2.setLayout(headerInside2Layout);
        headerInside2Layout.setHorizontalGroup(
            headerInside2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, headerInside2Layout.createSequentialGroup()
                .addContainerGap(260, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addGap(269, 269, 269))
        );
        headerInside2Layout.setVerticalGroup(
            headerInside2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(headerInside2Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(84, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(headerInside2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(headerInside2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 359, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void BtnCariPeminjamanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariPeminjamanActionPerformed
        // TODO add your handling code here:
        String entry = TxtCariPeminjaman.getText();
        CariDataPeminjaman(entry);
        this.requestFocus();
    }//GEN-LAST:event_BtnCariPeminjamanActionPerformed

    private void TablePeminjamanMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TablePeminjamanMouseClicked
        // TODO add your handling code here:
        int selectedRow = TablePeminjaman.getSelectedRow();
              
        Object nomor_isbn = TablePeminjaman.getValueAt(selectedRow, 1);
        Object nama_peminjam = TablePeminjaman.getValueAt(selectedRow, 2);
        Object judul_buku = TablePeminjaman.getValueAt(selectedRow, 3);
        
        TxtJudulBuku.setText(judul_buku.toString());
        TxtNamaPeminjam.setText(nama_peminjam.toString());
        TxtNomorISBN.setText(nomor_isbn.toString());
        
        TxtJudulBuku.setEnabled(false);
        TxtNamaPeminjam.setEnabled(false);
        TxtNomorISBN.setEnabled(false);
        
    }//GEN-LAST:event_TablePeminjamanMouseClicked

    private void BtnKembalikanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnKembalikanActionPerformed
        // TODO add your handling code here:
        String nomor_isbn = TxtNomorISBN.getText();
        String nama_peminjam = TxtNamaPeminjam.getText();
        String judul_buku = TxtJudulBuku.getText();
        Date tanggalPengembalian = TanggalPemgembalian.getDate();
        java.sql.Date tanggal_pengembalian = new java.sql.Date(tanggalPengembalian.getTime());
        
        KembalikanBuku(nomor_isbn, nama_peminjam, judul_buku, tanggal_pengembalian);
    }//GEN-LAST:event_BtnKembalikanActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PengembalianBuku.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PengembalianBuku.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PengembalianBuku.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PengembalianBuku.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PengembalianBuku().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnCariPeminjaman;
    private javax.swing.JButton BtnKembalikan;
    private javax.swing.JTable TablePeminjaman;
    private com.toedter.calendar.JDateChooser TanggalPemgembalian;
    private javax.swing.JTextField TxtCariPeminjaman;
    private javax.swing.JTextField TxtJudulBuku;
    private javax.swing.JTextField TxtNamaPeminjam;
    private javax.swing.JTextField TxtNomorISBN;
    private pbo.unsia.le.brarian.HeaderInside headerInside1;
    private pbo.unsia.le.brarian.HeaderInside headerInside2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
