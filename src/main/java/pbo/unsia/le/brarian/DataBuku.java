/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package pbo.unsia.le.brarian;

/**
 *
 * @author syarifmaulana
 */

import javax.swing.JOptionPane;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.table.DefaultTableModel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DataBuku extends javax.swing.JFrame {

    /**
     * Creates new form DataBuku
     */
    private Object globalBukuId;
    
    public DataBuku() {
        initComponents();
        DBConnection.openConnection();
        LoadBuku();
        this.setDefaultCloseOperation(PeminjamanBuku.DISPOSE_ON_CLOSE);
    }
    
    private void setColumnWidth() {
        TableBuku.getColumnModel().getColumn(0).setPreferredWidth(30);
        TableBuku.getColumnModel().getColumn(1).setPreferredWidth(50);
        TableBuku.getColumnModel().getColumn(2).setPreferredWidth(100);
        TableBuku.getColumnModel().getColumn(3).setPreferredWidth(100);
        TableBuku.getColumnModel().getColumn(4).setPreferredWidth(50);
        TableBuku.getColumnModel().getColumn(5).setPreferredWidth(50);
    }
    
    private void LoadBuku() {
        
        String sql = "SELECT * FROM buku ORDER BY id ASC";

        try {
            PreparedStatement st = DBConnection.getConnection().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = st.executeQuery();

            // Menambahkan kolom nomor pada header
            String[] header = {"No.", "No. ISBN", "Judul Buku", "Nama Penulis", "Tahun Terbit", "Jumlah"};

            rs.last();
            int baris = rs.getRow();
            rs.beforeFirst();

            Object[][] dttbl = new Object[baris][6]; // Jumlah kolom ditambah 1
            int curbaris = 0;
            int nomor = 1; // Variabel nomor untuk penomoran
            while (rs.next()) {
                dttbl[curbaris][0] = nomor; // Menyimpan nilai nomor pada kolom pertama
                dttbl[curbaris][1] = rs.getString("nomor_isbn");
                dttbl[curbaris][2] = rs.getString("judul_buku");
                dttbl[curbaris][3] = rs.getString("nama_penulis");
                dttbl[curbaris][4] = rs.getString("tahun_terbit");
                dttbl[curbaris][5] = rs.getString("jumlah_buku");
                curbaris++;
                nomor++; // Menaikkan nilai nomor setiap iterasi
            }
            TableBuku.setModel(new DefaultTableModel(dttbl, header));
            setColumnWidth();
            TxtNomorISBN.setEnabled(true);

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            System.out.println(e);
        }
    }
    
    
    private void SearchBuku(String entry) {
        
        String sql = "SELECT * FROM buku WHERE nomor_isbn LIKE '%" + entry + "%' OR judul_buku LIKE '%" + entry + "%' OR nama_penulis LIKE '%" + entry + "%';";

        try {
            PreparedStatement st = DBConnection.getConnection().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = st.executeQuery();

            // Menambahkan kolom nomor pada header
            String[] header = {"No.", "No. ISBN", "Judul Buku", "Nama Penulis", "Tahun Terbit", "Jumlah"};

            rs.last();
            int baris = rs.getRow();
            rs.beforeFirst();

            Object[][] dttbl = new Object[baris][6]; // Jumlah kolom ditambah 1
            int curbaris = 0;
            int nomor = 1; // Variabel nomor untuk penomoran
            while (rs.next()) {
                dttbl[curbaris][0] = nomor; // Menyimpan nilai nomor pada kolom pertama
                dttbl[curbaris][1] = rs.getString("nomor_isbn");
                dttbl[curbaris][2] = rs.getString("judul_buku");
                dttbl[curbaris][3] = rs.getString("nama_penulis");
                dttbl[curbaris][4] = rs.getString("tahun_terbit");
                dttbl[curbaris][5] = rs.getString("jumlah_buku");
                curbaris++;
                nomor++; // Menaikkan nilai nomor setiap iterasi
            }
            TableBuku.setModel(new DefaultTableModel(dttbl, header));
            setColumnWidth();
            TxtNomorISBN.setEnabled(true);

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            System.out.println(e);
        }
    }

    
    private void TambahBuku() {
        String sql = "INSERT INTO buku values(NULL, ?, ?, ?, ?, ?)";

        try {
            PreparedStatement st = DBConnection.getConnection().prepareStatement(sql);

            st.setString(1, TxtNomorISBN.getText());
            st.setString(2, TxtJudulBuku.getText());
            st.setString(3, TxtNamaPenulis.getText());

            // Mendapatkan tanggal dari JDateChooser
            Date tanggalTerbit = TahunTerbit.getDate();

            // Mengubah java.util.Date menjadi java.sql.Date
            java.sql.Date tanggalTerbitSQL = new java.sql.Date(tanggalTerbit.getTime());
            st.setDate(4, tanggalTerbitSQL);

            st.setString(5, String.valueOf((int) TxtJumlahBuku.getValue()));

            st.executeUpdate();
            JOptionPane.showMessageDialog(null, "Data Berhasil Disimpan.");

            // Mengosongkan JTextField
            TxtNomorISBN.setText("");
            TxtJudulBuku.setText("");
            TxtNamaPenulis.setText("");
            TahunTerbit.setDate(null); // Mengosongkan JDateChooser
            TxtJumlahBuku.setValue(0);
            
            LoadBuku();

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            System.out.println(e);
        }
    }

    
    private void EditBuku(Object id) {
        String sql = "UPDATE buku SET judul_buku=?, nama_penulis=?, tahun_terbit=?, jumlah_buku=? WHERE nomor_isbn='" + id + "'";

        try {
            PreparedStatement st = DBConnection.getConnection().prepareStatement(sql);

            st.setString(1, TxtJudulBuku.getText());
            st.setString(2, TxtNamaPenulis.getText());

            // Mendapatkan tanggal dari JDateChooser
            Date tanggalTerbit = TahunTerbit.getDate();

            // Mengubah java.util.Date menjadi java.sql.Date
            java.sql.Date tanggalTerbitSQL = new java.sql.Date(tanggalTerbit.getTime());
            st.setDate(3, tanggalTerbitSQL);

            st.setString(4, String.valueOf((int) TxtJumlahBuku.getValue()));

            st.executeUpdate();
            JOptionPane.showMessageDialog(null, "Data Berhasil Dirubah.");

            // Mengosongkan JTextField
            TxtNomorISBN.setText("");
            TxtJudulBuku.setText("");
            TxtNamaPenulis.setText("");
            TahunTerbit.setDate(null); // Mengosongkan JDateChooser
            TxtJumlahBuku.setValue(0);
            
            LoadBuku();

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            System.out.println(e);
        }
    }

    
    private void HapusBuku(Object id) {
    String sql = "DELETE FROM buku WHERE nomor_isbn='" + id + "'";

        try {
            PreparedStatement st = DBConnection.getConnection().prepareStatement(sql);

            st.executeUpdate();
            JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus.");

            // Mengosongkan JTextField
            TxtNomorISBN.setText("");
            TxtJudulBuku.setText("");
            TxtNamaPenulis.setText("");
            TahunTerbit.setDate(null); // Mengosongkan JDateChooser
            TxtJumlahBuku.setValue(0);
            
            LoadBuku();

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            System.out.println(e);
        }
    }

    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TableBuku = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        TxtCariBuku = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        TxtJudulBuku = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        TxtNamaPenulis = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        TxtJumlahBuku = new javax.swing.JSpinner();
        BtnHapusBuku = new javax.swing.JButton();
        BtnCariBuku = new javax.swing.JButton();
        BtnEditBuku = new javax.swing.JButton();
        TxtNomorISBN = new javax.swing.JTextField();
        BtnTambahBuku = new javax.swing.JButton();
        TahunTerbit = new com.toedter.calendar.JDateChooser();
        headerInside1 = new pbo.unsia.le.brarian.HeaderInside();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Data Buku");
        setBackground(new java.awt.Color(38, 40, 22));
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(38, 40, 22));
        jPanel1.setForeground(new java.awt.Color(255, 255, 255));
        jPanel1.setToolTipText("");
        jPanel1.setLayout(null);

        TableBuku.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        TableBuku.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TableBukuMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(TableBuku);

        jPanel1.add(jScrollPane1);
        jScrollPane1.setBounds(30, 50, 520, 290);

        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Nomor ISBN");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(570, 10, 250, 16);
        jPanel1.add(TxtCariBuku);
        TxtCariBuku.setBounds(30, 10, 410, 30);

        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Judul Buku");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(570, 80, 250, 16);
        jPanel1.add(TxtJudulBuku);
        TxtJudulBuku.setBounds(570, 100, 250, 30);

        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Nama Penulis");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(570, 150, 250, 16);
        jPanel1.add(TxtNamaPenulis);
        TxtNamaPenulis.setBounds(570, 170, 250, 30);

        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Tahun Terbit");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(570, 220, 250, 16);

        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Jumlah Buku");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(570, 290, 250, 16);
        jPanel1.add(TxtJumlahBuku);
        TxtJumlahBuku.setBounds(570, 310, 250, 30);

        BtnHapusBuku.setText("Hapus Data");
        BtnHapusBuku.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnHapusBukuActionPerformed(evt);
            }
        });
        jPanel1.add(BtnHapusBuku);
        BtnHapusBuku.setBounds(840, 130, 120, 30);

        BtnCariBuku.setText("Cari");
        BtnCariBuku.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCariBukuActionPerformed(evt);
            }
        });
        jPanel1.add(BtnCariBuku);
        BtnCariBuku.setBounds(450, 10, 100, 30);

        BtnEditBuku.setText("Edit Data");
        BtnEditBuku.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditBukuActionPerformed(evt);
            }
        });
        jPanel1.add(BtnEditBuku);
        BtnEditBuku.setBounds(840, 80, 120, 30);

        TxtNomorISBN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtNomorISBNActionPerformed(evt);
            }
        });
        jPanel1.add(TxtNomorISBN);
        TxtNomorISBN.setBounds(570, 30, 250, 30);

        BtnTambahBuku.setText("Tambah Data");
        BtnTambahBuku.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnTambahBukuActionPerformed(evt);
            }
        });
        jPanel1.add(BtnTambahBuku);
        BtnTambahBuku.setBounds(840, 30, 120, 30);
        jPanel1.add(TahunTerbit);
        TahunTerbit.setBounds(570, 240, 250, 30);

        jLabel1.setFont(new java.awt.Font("Helvetica Neue", 1, 36)); // NOI18N
        jLabel1.setText("Data Buku");

        javax.swing.GroupLayout headerInside1Layout = new javax.swing.GroupLayout(headerInside1);
        headerInside1.setLayout(headerInside1Layout);
        headerInside1Layout.setHorizontalGroup(
            headerInside1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, headerInside1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(392, 392, 392))
        );
        headerInside1Layout.setVerticalGroup(
            headerInside1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(headerInside1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(88, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1000, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addComponent(headerInside1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(headerInside1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void BtnHapusBukuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnHapusBukuActionPerformed
        // TODO add your handling code here:
         if(globalBukuId != null) {
            HapusBuku(globalBukuId);
            this.requestFocus();
        }
        else {
            JOptionPane.showMessageDialog(null, "Pilih data dari tabel terlebih dahulu!");
        }
    }//GEN-LAST:event_BtnHapusBukuActionPerformed

    private void BtnCariBukuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCariBukuActionPerformed
        // TODO add your handling code here:
        String entry = TxtCariBuku.getText();
        SearchBuku(entry);
        this.requestFocus();
    }//GEN-LAST:event_BtnCariBukuActionPerformed

    private void BtnEditBukuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditBukuActionPerformed
        // TODO add your handling code here:
        if(globalBukuId != null) {
            EditBuku(globalBukuId);
            this.requestFocus();
        }
        else {
            JOptionPane.showMessageDialog(null, "Pilih data dari tabel terlebih dahulu!");
        }
    }//GEN-LAST:event_BtnEditBukuActionPerformed

    private void BtnTambahBukuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnTambahBukuActionPerformed
        // TODO add your handling code here:
        TambahBuku();
        this.requestFocus();
        LoadBuku();
    }//GEN-LAST:event_BtnTambahBukuActionPerformed

    private void TxtNomorISBNActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtNomorISBNActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TxtNomorISBNActionPerformed

    private void TableBukuMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TableBukuMouseClicked
        // TODO add your handling code here:
        int selectedRow = TableBuku.getSelectedRow();
        
        globalBukuId = TableBuku.getValueAt(selectedRow, 1);
        
        Object nomor_isbn = TableBuku.getValueAt(selectedRow, 1);
        Object judul_buku = TableBuku.getValueAt(selectedRow, 2);
        Object nama_penulis = TableBuku.getValueAt(selectedRow, 3);
        Object tahun_terbit = TableBuku.getValueAt(selectedRow, 4);
        Object jumlah_buku = TableBuku.getValueAt(selectedRow, 5);
        
        TxtNomorISBN.setText(nomor_isbn.toString());
        TxtNomorISBN.setEnabled(false);
        TxtJudulBuku.setText(judul_buku.toString());
        TxtNamaPenulis.setText(nama_penulis.toString());
        TxtJumlahBuku.setValue(Integer.parseInt(jumlah_buku.toString()));
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date tanggalTerbit = dateFormat.parse(tahun_terbit.toString());
            TahunTerbit.setDate(tanggalTerbit);
        } catch (ParseException ex) {
            // Tangkap pengecualian ParseException
            ex.printStackTrace();
        }
    }//GEN-LAST:event_TableBukuMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DataBuku.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DataBuku.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DataBuku.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DataBuku.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new DataBuku().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnCariBuku;
    private javax.swing.JButton BtnEditBuku;
    private javax.swing.JButton BtnHapusBuku;
    private javax.swing.JButton BtnTambahBuku;
    private javax.swing.JTable TableBuku;
    private com.toedter.calendar.JDateChooser TahunTerbit;
    private javax.swing.JTextField TxtCariBuku;
    private javax.swing.JTextField TxtJudulBuku;
    private javax.swing.JSpinner TxtJumlahBuku;
    private javax.swing.JTextField TxtNamaPenulis;
    private javax.swing.JTextField TxtNomorISBN;
    private pbo.unsia.le.brarian.HeaderInside headerInside1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
